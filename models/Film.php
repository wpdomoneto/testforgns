<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Films".
 *
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property boolean $isActive
 */
class Film extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Films';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'year'], 'required'],

            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'match', 'pattern' => '#^[\w-]+$#i'],
            ['name', 'string', 'min' => 2],

            ['year', 'filter', 'filter' => 'trim'],
            ['year', 'match', 'pattern' => '#^[0-9]{4}$#i'],
            ['year', 'integer'],

            [['isActive'], 'boolean'],

        ];
    }
}
