<?php

/* @var $this yii\web\View */
/* @var $model app\models\Film */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create';
?>
<div class="row">
    <div class="col-lg-5">
        <div class="h2 text-center">Create</div>

        <?php $form = ActiveForm::begin(['id' => 'film-form']); ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'year') ?>
        <?= $form->field($model, 'isActive')->dropDownList(['1'=>'Показать','0'=>'Скрыть']) ?>
        <div class="form-group">
            <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>