<?php

/* @var $this yii\web\View */
/* @var $dataProvider object*/
use yii\grid\GridView;

$this->title = 'testFilmsGNS';
?>
<div class="row">
    <div class="h2 text-center">Films</div>
    <div class="col-lg-12">
        <a class="btn btn-lg btn-info" href="/create">Create</a>
    </div>
</div>

<?
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'=>[
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'year',
    ]
]);?>