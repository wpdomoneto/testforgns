<?php

namespace app\controllers;

use app\models\Film;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;


class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Film::find()->where(['isActive'=>1]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('index',
                ['dataProvider'=>$dataProvider]);
    }

    public function actionCreate()
    {
        $model =  new Film();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            return $this->goHome();
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

}
